const IMAGES = [];
const NEW_IMAGES = [];
const WRONG_VALUE = ['9_1447691013_3726.png',
                     '9_1423150010_6441.png',
                     '9_1423149959_5909.png',
                     '9_1423149908_5874.png',
                     '9_1528213223_6374.jpg',
                     '9_1527670984_3732.jpg',];

IMAGES.forEach((image, index) => {
    if (index > 2 && image.size < 5242880 && image.width > 880 && WRONG_VALUE.indexOf(image.filename) === -1) {
        NEW_IMAGES.push(image);
    }
});


